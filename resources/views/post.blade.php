@extends('layout')

@section('content')
    <!-- about us image -->
    <section>
        <div id="about-us-header" style="background: url({{asset('img/slider.png')}});">
            <div class="container">
                <div class="row">
                        <div class="col-sm-8 offset-sm-2"> 
                            <div class="header-text-wrap">
                                <div class="inner-header-text">
                                    <h1>{{ $post->category }}</h1>
                                </div>
                            </div>
                        </div>                     
                </div>
            </div>
        </div>
    </section>
    <!-- end about us image -->
    <!-- single detail -->
    <section id="single-pg">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                        @if($post->feature_image==null)
                                            <img src="{{ asset("/img/logo.png") }}" class="img-fluid blog-img" alt="{{$post->title }}">           
                                        @else
                                            <img src="{{ asset($post->feature_image) }}" class="img-fluid blog-img" alt="{{$post->title }}">
                                        @endif
                    <div class="text-left">
                        <h3 class="mt-3">{{ $post->title }}</h3>
                        <p class="blog-date">{{ $post->created_at->toFormattedDateString() }}</p>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-share-square"></i>
                    </div>
                    <div class="blog-content">
                        {!! $post->content !!}
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                   <div id="news" class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="new-wrap mt-5">
                                    <div class="mt-3 mb-5">
                                        <h3 class="text-center">Recent&nbsp;{{ title_case($post->category) }}</h3>
                                    </div>
                                    <ul class="list-unstyled">
                                        @foreach($otherPosts as $post)
                                            <a href="/{{ $post->category.'/'.$post->slug }}" title="{{ $post->title }}">
                                                <li class="media mb-3">
                                                    @if($post->feature_image==null)
                                                        <img src="{{ asset("/img/logo.png") }}" class="mr-3 img-fluid" alt="{{ $post->title }}"  style="width: 64px; height: 64px;">
                                                    @else
                                                        <img class="mr-3 img-fluid" src="{{ asset($post->feature_image) }}" alt="{{ $post->title }}" style="width: 64px; height: 64px;">
                                                    @endif
                                                    <div class="media-body">
                                                      <h5 class="mt-0 mb-1">{{ $post->title }}</h5>
                                                      <p>{{ $post->created_at->toFormattedDateString() }}</p>
                                                    </div>
                                                </li>
                                            </a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end single detail -->
@endsection