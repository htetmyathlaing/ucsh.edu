@extends('layout')

@section('content')
	<!-- </section> -->
	<!-- end whole-wapper -->
	<!-- slider -->
	<section class="container-fluid slider-image-container-fluid" data-aos="fade-up">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		  	<ol class="carousel-indicators">
		    	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  	</ol>
		  	<div class="carousel-inner">
		    	<div class="carousel-item active">
		      		<img class="d-block w-100" src="{{ asset('img/slider.png') }}" alt="First slide">
		      		<div class="carousel-caption" data-aos="flip-left">
	                	<div class="para"> 
	                  		<p>မောင်မယ်သစ်လွင်များအား</p>
	                  		<span>လှိုက်လဲစွာကြိုဆိုပါသည်။</span>
						</div>
	            	</div>
		    	</div>
			    <div class="carousel-item">
			      	<img class="d-block w-100" src="{{ asset('img/slider.png') }}" alt="Second slide">
			      	<div class="carousel-caption">
		                <div class="para"> 
		                  	<p>မောင်မယ်သစ်လွင်များအား</p>
		                  	<span>လှိုက်လဲစွာကြိုဆိုပါသည်။</span>
						</div>
		            </div>
			    </div>
			    <div class="carousel-item">
			      	<img class="d-block w-100" src="{{ asset('img/slider.png') }}" alt="Third slide">
			      	<div class="carousel-caption">
		                <div class="para"> 
		                  	<p>မောင်မယ်သစ်လွင်များအား</p>
		                  	<span>လှိုက်လဲစွာကြိုဆိုပါသည်။</span>
						</div>
		            </div>
			    </div>
			</div>
		
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			</a>
		</div>
	</section>
	<!-- end slider -->

	<section class="container-fluid">
		<div class="row">
			<div class="col-12 col-lg-8">
				<!-- last new -->
				<section class="container-fluid last-bgcolor p-3" data-aos="flip-up">
					<h2 class="text-center">Last News</h2>
					<div class="new-link text-right">
						<a href="/news">View All</a>
					</div>
					
					<div class="row mt-4 mb-4">

						@foreach($news as $new)
							<div class="col-md-6 mb-3">
								<div class="new-box">
									<div class="text-center">
										@if($new->feature_image==null)
	                    					<img src="{{ asset("/img/logo.png") }}" class="img-fluid latest-news" alt="{{$new->title }}">			
	                    				@else
											<img src="{{ asset($new->feature_image) }}" class="img-fluid latest-news" alt="{{$new->title }}">
										@endif
									</div>
									
									<div class="p-4 text-bgcolor">
										<h5><a href="/{{ $new->category.'/'.$new->slug }}">{{ $new->title }}</a></h5>
										<p class="new-date">{{ $new->created_at->toFormattedDateString() }}</p>
										<p>{{ str_limit(strip_tags($new->content), 250, '...') }} </p>
										<div class="text-center">
											<a href="/{{ $new->category.'/'.$new->slug }}" class="text-center">
												<button class="button button--rayen button--border-thin button--text-thick button--text-upper button--size-s" data-text="Read More"><span>Read More</span></button>
											</a>
										</div>
									</div>
								</div>
							</div>
						@endforeach

					</div>
				</section>
				<!-- end last new -->


				<!-- activities -->
				{{-- <section class="container mt-4 mb-4">
					<h2 class="text-center">Activities</h2>
					<div class="row mt-4">
						<div class="col-md-6 order-2 order-md-1">
							<h3 class="mt-4 mt-md-0 mt-lg-3"> Fresher Welcome</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</p>
						</div>
						<div class="col-md-6 order-1 order-md-2">
							<img src="{{ asset('img/fresher.png') }}" class="img-fluid activites-img">
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<img src="{{ asset('img/fresher.png') }}" class="img-fluid activites-img">
						</div>
						<div class="col-md-6">
							<h3 class="mt-4 mt-md-0 mt-lg-3"> Fresher Welcome</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</p>
						</div>
					</div>
				</section> --}}
				<!-- end activities -->
			</div>
			
			<div class="col-12 col-lg-4">
				<section id="new-slider-bar">
					<div id="news" class="container">
						<div class="row">
							<div class="col-12">
								<div class="new-wrap mt-5">
									<div class="mt-3 mb-5">
										<h3 class="text-center">Recent Activities</h3>
									</div>
									<ul class="list-unstyled">
										@foreach($activities as $activity)
											<a href="/{{ $activity->category.'/'.$activity->slug }}" title="{{$activity->title }}">
												<li class="media mb-3">
													@if($activity->feature_image==null)
                        								<img src="{{ asset("/img/logo.png") }}" class="mr-3 img-fluid" alt="{{$activity->title }}" style="width: 64px; height: 64px;">
                        							@else
												    	<img class="mr-3 img-fluid" 
												    	src="{{ asset($activity->feature_image) }}" 	
												    	alt="{{ $activity->title }}"
												    	style="width: 64px; height: 64px;">
												    @endif
												    <div class="media-body">
												    	<h5 class="mt-0 mb-1">{{ $activity->title }}</h5>
												      	<p> {{ $activity->created_at->toFormattedDateString() }} </p>
												    </div>
											  	</li>
										  	</a>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div id="anu" class="container">
						<div class="row">
							<div class="col-12">
								<div class="new-wrap mt-5">
									<div class="mt-3 mb-5">
										<h3 class="text-center">Recent Announcements</h3>
									</div>
									<ul class="list-unstyled">
										@foreach($announcements as $announcement)
											<a href="/{{ $announcement->category.'/'.$announcement->slug }}" title="{{$announcement->title }}">
												<li class="media mb-3">
													@if($announcement->feature_image==null)
                        								<img src="{{ asset("/img/logo.png") }}" class="mr-3 img-fluid" style="width: 64px; height: 64px;" alt="{{$announcement->title }}">
                        							@else
												    	<img class="mr-3 img-fluid" 
												    	src="{{ asset($announcement->feature_image) }}" 	
												    	alt="{{ $announcement->title }}" 
												    	style="width: 64px; height: 64px;" alt="{{$announcement->title }}">
												    @endif
												    <div class="media-body">
												    	<h5 class="mt-0 mb-1">{{ $announcement->title }}</h5>
												      	<p> {{ $announcement->created_at->toFormattedDateString() }} </p>
												    </div>
											  	</li>
										  	</a>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
@endsection

@section('css')
	.latest-news{
		{{-- width: 100% !important; --}}
		height: 250px !important;
	}
@endsection

