<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UCSH</title>

        <link rel="icon" type="image/png" href="{{ asset('/img/logo.png') }}">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />
        
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/aos.css') }}">

        <link rel="stylesheet" type="text/css" href="/css/style.css">

        <style>
             @yield('css')
         </style> 

    </head>
    <body>
        <!-- header -->
        <section id="header" class="container-fluid home-bgcolor p-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="media">

                            <a href="/">
                                <img class="align-self-start mr-3 header-logo" src="/img/logo.png" alt="UCSH logo">
                            </a>
                              <div class="media-body text-center">
                                <h1 class="header-title">University Of Computer Studies, Hinthada</h1>
                                <h1 class="mt-0 header-title">ကွန်ပျူတာတက္ကသိုလ် (ဟင်္သာတ)</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end header -->

        <!-- menu bar -->
        <header>
            <div class="container-fluid">
                <div class="header-nav">
                    <div class="menu-header">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul id="menu-main" class="navbar-nav mx-auto">
                                    <li class="nav-item">
                                        <a title="Home" href="/" class="nav-link">Home</a>
                                    </li>

                                    <li class="nav-item">
                                        <a title="Academic" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Academic</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a title="Bachelor Degree" href="/degrees/bachelor-degree" class="dropdown-item">Bachelor Degree</a>
                                            </li>
                                            <li class="nav-item">
                                                <a title="Master Degree" href="/degrees/master-degree" class="dropdown-item">Master Degree</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a title="Departments" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Departments</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a title="Myanmar Department" href="/departments/faculty-of-myanmar" class="dropdown-item">Faculty of Myanmar</a>
                                            </li>
                                            <li class="nav-item">
                                                <a title="English Department" href="/departments/faculty-of-english" class="dropdown-item">Faculty of English</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Natural Science" href="/departments/faculty-of-natural-science" class="dropdown-item">Faculty of Natural Science</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Faculty of Computing" href="/departments/faculty-of-computing" class="dropdown-item">Faculty of Computing</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Faculty of Computer Science" href="/departments/faculty-of-computer-science" class="dropdown-item">Faculty of Computer Science</a>
                                            </li>
                                            <li class="nav-item">
                                                <a title="Faculty of Information Science" href="/departments/faculty-of-computer-information-science" class="dropdown-item">Faculty of Information Science</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Faculty of Computer System and Technologies" href="/departments/faculty-of-computer-system-and-technology" class="dropdown-item">Faculty of Computer System and Technologies</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Finance" href="/departments/finance" class="dropdown-item">Finance</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Library Department" href="/departments/library" class="dropdown-item">Library Department</a>
                                            </li>

                                            <li class="nav-item">
                                                <a title="Student Affair" href="/departments/student-affair" class="dropdown-item">Student Affair</a>
                                            </li>
                                            <li class="nav-item">
                                                <a title="Maintenance" href="/departments/faculty-of-computer-information-technology-support-and-maintainance" class="dropdown-item">Faculty of Computer Information Technology Support and Maintainance</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <a title="News" href="/news" class="nav-link">News & Events & Announment</a>
                                    </li>

                                    <li class="nav-item">
                                        <a title="ContactUs" href="/contact-us" class="nav-link">Contact Us</a>
                                    </li>

                                    <li class="nav-item m-0">
                                        <a title="About Us" href="/about-us" class="nav-link">About Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- end menu bar -->

        @yield('content')

        <!-- footer -->
        <section class="home-bgcolor p-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6 text-center">
                        <p>Copyright &copy; <?php echo date("Y"); ?> University of Computer Studies ,Hinthada. All Rights Reserved.</p>
                    </div>
                    <div class="col-12 col-md-6">
                        <ul>
                            <li class="media">
                              <div class="mr-2">
                                <i class="fab fa-facebook" aria-hidden="true" style="font-size:30px"></i>
                              </div>
                              <div class="media-body my-auto">
                                <span>
                                  <a href="https://www.facebook.com/ucshofficial/" target="_blank">Like up on Facebook</a>
                                </span>
                              </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- end footer -->
       
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/aos.js') }}"></script>
        <script src="{{ asset('/js/script.js') }}"></script>

        @yield('js')

    </body>
</html>
