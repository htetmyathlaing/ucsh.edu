<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Blade;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('administer', function($user){
            return $user->role->name === 'Admin';
        });

        Blade::directive('role', function($role){
            return "<?php if(Auth::user()->role->name === $role): ?>";
        });

        Blade::directive('endrole', function(){
            return "<?php endif; ?>";
        });
    }
}
