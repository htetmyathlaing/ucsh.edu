
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
// import VueContentPlaceholders from 'vue-content-placeholders'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueRouter)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('side-bar', require('./components/SideBar.vue'));
Vue.component('nav-bar', require('./components/NavBar.vue'));
Vue.component('line-chart', require('./components/LineChart.vue'));
Vue.component('bar-chart', require('./components/BarChart.vue'));
Vue.component('todo-list', require('./components/ToDoList.vue'));
Vue.component('summernote-editor', require('./components/Editor.vue'));
Vue.component('main-layout', require('./components/Main.vue'));


import Dashboard from './components/Dashboard.vue'
import Posts from './components/Posts.vue'
import Pages from './components/Pages.vue'
import MailBox from './components/MailBox.vue'
import Account from './components/Account.vue'
import Users from './components/Users.vue'
import Media from './components/Media.vue'

const routes = [
    {path: '/', component: Dashboard },
    {path: '/posts', component: Posts },
    {path: '/pages', component: Pages },
    {path: '/mailbox', component: MailBox },
    {path: '/account', component: Account },
    {path: '/users', component: Users },
    {path: '/media', component: Media }
]

const router = new VueRouter({
    routes
})

const store = new Vuex.Store({
	state: {
	    posts: [],
	    pages: [],
		mails: [],
		medias: [],
		toDoList: [],
		user: window.user,
		isAdmin: window.user.role.name === 'Admin' ? true : false,
		roles: window.roles,
		users: []
	},
	mutations: {
		assigntoDoList(state, toDoList){
			state.toDoList = toDoList
		},
		addNewToDoItem(state, toDoItem){
			state.toDoList.unshift(toDoItem)
		},
		editToDoItem(state, toDoItem){
			let index = state.toDoList.findIndex( item => item.id == toDoItem.id )
			state.toDoList[index] = toDoItem
		},
		deleteToDoItem(state, toDoItem){
			state.toDoList = state.toDoList.filter( item => item.id != toDoItem.id )
		},
		assignPosts(state, posts){
			state.posts = posts
		},
		addNewPost(state, post){
			// add new post
			state.posts.unshift(post)
		},
		editPost(state, post){
			// edit post content
			let index = state.posts.findIndex( p => p.id == post.id )
			state.posts[index] = post
		},
		trashPost(state, id){
			// move post to trash
			state.posts.find( p => p.id == id ).type = 'trash'
		},
		deletePost(state, id){
			// permently delete
			state.posts = state.posts.filter( p => p.id != id )
		},
		restorePost(state, id){
			// move post to draft
			state.posts.find( p => p.id == id ).type = 'draft'
		},
		bulkTrashPosts(state, ids){
			ids.forEach( (id ) => {
				state.posts.find( p => p.id == id ).type = 'trash'
			})
		},
		bulkRestorePosts(state, ids){
			ids.forEach( (id ) => {
				state.posts.find( p => p.id == id ).type = 'draft'
			})
		},
		bulkDeletePosts(state, ids){
			ids.forEach( (id ) => {
				state.posts = state.posts.filter( p => p.id != id )
			})
		},
		assignPages(state, pages){
			state.pages = pages
		},
		editPage(state, page){
			// edit post content
			let index = state.pages.findIndex( p => p.id == page.id )
			state.pages[index] = page
		},
		assignMails(state, mails){
			state.mails = mails
		},
		deleteMail(state, id ){
			state.mails = state.mails.filter( m => m.id != id )
		},
		bulkDeleteMails(state, ids){
			ids.forEach( (id ) => {
				state.mails = state.mails.filter( m => m.id != id )
			})
		},
		assignUsers(state, users){
			state.users = users
		},
		addNewUser(state, user){
			state.users.unshift(user)
		},
		editUser(state, user){
			let index = state.users.findIndex( u => u.id == user.id )
			state.users[index] = user
		},
		deleteUser(state, user){
			state.users = state.users.filter( u => u.id != user.id )
		},
		assignMedias(state, medias){
			state.medias = medias
		},
		addNewMedia(state, media){
			state.medias.unshift(media)
		},
		deleteMedia(state, id ){
			state.medias = state.medias.filter( m => m.id != id )
		},
		bulkDeleteMedias(state, ids){
			ids.forEach( (id ) => {
				state.medias = state.medias.filter( m => m.id != id )
			})
		},

	}
})

const app = new Vue({
    el: '#app',
    router,
    store,
});
