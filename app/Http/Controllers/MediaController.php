<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Media::latest()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('media')->getClientSize() > 104857600)
            return 'false';
        
        // $media_title = $request->file('media')->getClientOriginalName();
        // $media_extension = $request->file('media')->getClientOriginalExtension(); 
        // $media_name = $media_title.'.' .$media_extension;
        $media_name = $request->file('media')->getClientOriginalName();
        $path = $request->file('media')->storeAs('public/', $media_name);

        $media = new Media();
        $media->name = $media_name;
        $media->type = $request->file('media')->getClientMimeType();
        $media->uploaded_by = Auth::user()->name;
        $media->save();
        return $media;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $media)
    {
        Storage::delete('public\/'.$media->name);
        $media->delete();
        return 'true';
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDelete(Request $request)
    {
        $ids = $request->medias;
        foreach ($ids as $id) {
           $media = Media::findOrFail($id);
           Storage::delete('public\/'.$media->name);
           $media->delete(); 
        }
        return "true"; 
    }
}
