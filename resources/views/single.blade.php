@extends('layout')

@section('content')
    <!-- about us image -->
    <section>
        <div id="about-us-header" style="background: url({{asset('img/slider.png')}});">
            <div class="container">
                <div class="row">
                        <div class="col-sm-8 offset-sm-2"> 
                            <div class="header-text-wrap">
                                <div class="inner-header-text">
                                        <h1>Post Title</h1>
                                </div>
                            </div>
                        </div>                     
                </div>
            </div>
        </div>
    </section>
    <!-- end about us image -->
    <!-- single detail -->
    <section id="single-pg">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <img src="{!!asset('img/fresher.png')!!}" class="img-fluid blog-img">
                    <h3 class="mt-3">Post Title</h3>
                    <p class="blog-date">Oct 17 2018</p>
                    <div class="blog-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                   <div id="news" class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="new-wrap mt-5">
                                    <div class="mt-3 mb-5">
                                        <h3 class="text-center">News Recent</h3>
                                    </div>
                                    <ul class="list-unstyled">
                                    <?php for( $i=0 ; $i<4;$i++){ ?>
                                        <li class="media mb-3">
                                            <img class="mr-3 img-fluid" src="{!!asset('img/logo.png')!!}" alt="Generic placeholder image" style="width: 64px; height: 64px;">
                                            <div class="media-body">
                                              <h5 class="mt-0 mb-1">Programming Content</h5>
                                              <p> September 15 2018</p>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end single detail -->
@endsection