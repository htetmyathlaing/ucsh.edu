<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>UCSH | Admin Panel</title>

        <link rel="icon" type="image/png" href="{{ asset('img/logo.png') }}">
        {{-- Bootstrap --}}
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --}}
        {{-- Font Awesome --}}
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        {{-- Summer Note --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script> --}}

        {{-- Toastr Js --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" defer>

        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />

        <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
        <script src="{{ mix('/js/app.js') }}" defer></script>
    
        <style type="text/css" media="screen">
            .fade-complete-item {
                transition: all 0.1s;
            }
            .fade-enter-active, .fade-leave-active {
                transition: all 0.1s;
            }
            .fade-enter, .fade-leave-to{
                opacity: 0;
            }
            body{
                font-size: 1.05em;
                background: #F1F1F1;
            }
            footer{
                position: fixed;
                bottom: 0;
            }
        </style>
        <script>
               
            @role('Admin')
            {{-- @if(Auth::user()->can('administer')) --}}
                window.user = {!! Auth::user() !!}
                window.roles = {!! App\Role::all() !!}
            @else
                window.user = {!! Auth::user() !!}
            @endrole

    </script>

    </head>
    <body>

        <div id="app">
            {{-- <nav-bar></nav-bar>
            <side-bar  style="float: left;"></side-bar>
            <div>
                <transition name="fade">
                    <router-view></router-view>
                </transition>
                <footer>Develped by Htet Myat Hlaing</footer> 
            </div> --}}
            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                @csrf
            </form>     

            <main-layout></main-layout> 
        </div>

        {{-- JQuery --}}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        {{-- Popper JS
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> --}}
        {{-- Bootstrap JS --}}
        {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> --}}
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>

    </body>
</html>
