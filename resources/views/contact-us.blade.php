@extends('layout')

@section('content')
  <section id="contact-header" style="background: url({{asset('img/slider.png')}});">
     <div class="container">
          <div class="row">
               <div class="col-sm-8 offset-sm-2"> 
                    <div class="header-text-wrap">
                         <div class="inner-header-text">
                            <h1>Contact Us</h1>  <!-- fill data -->
                         </div>
                    </div>
               </div>                     
          </div>
     </div>
  </section>
  <!-- end about us image -->
  <!-- contact form -->
  <section id="contact-pg" class="py-5">
    <div class="container contact-info">
      <div class="row">
        <!-- contact form -->
        <div class="col-md-6 col-12">
          <h2 class="text-center">Contact Form</h2>
          @if(Session::has('flash_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Message sent successful!</strong> 
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif

          <form action="{{ url('mails' )}}" method="post" accept-charset="utf-8">
            @csrf
             <div>
                <div class="row">
                  <div class="col-12 col-md-6 form-group">
                    <input type="text" placeholder="Name" class="form-control" name="name">
                  </div>
                  <div class="col-12 col-md-6 form-group">
                    <input type="email" placeholder="Email*" class="form-control" name="email" required/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-6 form-group">
                    <input type="digit" placeholder="Phone" class="form-control" name="phone">
                  </div>
                  <div class="col-12 col-md-6 form-group">
                    <input type="text" placeholder="Subject *" class="form-control" name="subject" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                    <textarea class="form-control" placeholder="Message *" rows="5" name="message" required/></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group">
                  <input type="submit" name="" value="Send" class="btn" style="padding: 10px 60px; background: #7dc7f6;border-radius: 20px;">
                  </div>
                </div>
            </div>
          </form>
        </div>
        <!-- end contact form -->
        <div class="col-md-6 col-12">
          <h2 class="text-center mb-3">Contact Info</h2>
          <ul class="list-unstyled">
            <li class="media">
              <div class="mr-2">
                <i class="fas fa-map-marker-alt" aria-hidden="true" style="font-size:30px"></i>
              </div>
              <div class="media-body">
                <p>Univesity Of Computer Studies, Hinthada</p>
              </div>
            </li>
            <li class="media my-4">
              <div class="mr-2">
                <i class="far fa-envelope" aria-hidden="true" style="font-size:30px"></i>
              </div>
              <div class="media-body media-mail">
                <p>
                  <a href="mailto:moehtetnaing.ucsh@gmail.com" target="_blank">moehtetnaing.ucsh@gmail.com</a>
                </p>
              </div>
            </li>
            <li class="media">
              <div class="mr-2">
                <i class="fas fa-phone" aria-hidden="true" style="font-size:30px"></i>
              </div>
              <div class="media-body">
                <p>
                  <a href="tel:09253339255">09253339255</a>
                </p>
              </div>
            </li>
            <li class="media my-4">
              <div class="mr-2">
                <i class="fab fa-facebook" aria-hidden="true" style="font-size:30px"></i>
              </div>
              <div class="media-body media-mail">
                <p>
                  <a href="https://www.facebook.com/ucshofficial/" target="_blank">Like up on Facebook</a>
                </p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
 </section>
  <!-- end contact form -->
  <!-- google map -->
  <section> 
    <div id="map"></div>  
  </section>
  <!-- end google map -->
@endsection

@section('js')
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC44n4EJxputPRoWzorOaszqW-dFoVN8UE&callback=initMap"
          async defer></script>
  <script type="text/javascript">
    function initMap(){
      var location = {lat: 17.655474, lng: 95.457941};
      var map = new google.maps.Map(document.getElementById("map"),{
        zoom: 17,
        center: location,

        styles: [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#242f3e"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#746855"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#242f3e"
              }
            ]
          },
          {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#d59563"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#d59563"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#263c3f"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#6b9a76"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#38414e"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#212a37"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9ca5b3"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#746855"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#1f2835"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#f3d19c"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#2f3948"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#d59563"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#17263c"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#515c6d"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#17263c"
              }
            ]
          }
        ]
      });
      var marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: 'img/map-logo.png'
        // icon: 'img/logo.png'
      });
    }
  </script>
@endsection