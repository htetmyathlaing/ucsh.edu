@extends('layout')

@section('content')
    <!-- about us image -->
    <section>
        <div id="about-us-header" style="background: url(img/slider.png);">
            <div class="container">
                <div class="row">
                        <div class="col-sm-8 offset-sm-2"> 
                            <div class="header-text-wrap">
                                <div class="inner-header-text">
                                        <h1>Activities</h1>
                                </div>
                            </div>
                        </div>                     
                </div>
            </div>
        </div>
    </section>
    <!-- end about us image -->
    <!-- activity blog -->
    <section id="activity-pg">
        <div class="container">
            <div class="row">
                <?php for($i=1;$i<11;$i++){ ?>
                <div class="col-12 col-md-6 col-lg-4 mt-3 mb-3">
                    <div class="card-wrap">
                        <div class="card">
                        <img class="card-img-top" src="img/news.png" class="img-fluid" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <h6 class="card-date">September 16, 2018</h6>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="card-link">View</a>
                        </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- end new blog -->
@endsection