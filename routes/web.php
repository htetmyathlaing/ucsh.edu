<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Post;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


Auth::routes(['verify' => true]);

Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', function (Request $request) {
        return view('Admin.dashboard');
    });
});

Route::group(['prefix' => 'api', 'middleware' => ['auth']],function(){

    Route::get('dashboard/init', 'DashboardController@init');

    Route::get('dashboard/yearly-traffic/{year}', 'DashboardController@yearlyTraffic');

    Route::get('dashboard/monthly-traffic/{year}/{month}', 'DashboardController@monthlyTraffic');

    Route::get('dashboard/yearly-posts-update-rate/{year}', 'DashboardController@yearlyPostsUpdateRate');

     Route::get('dashboard/monthly-posts-update-rate/{year}/{month}', 'DashboardController@monthlyPostsUpdateRate');

    Route::put('posts/trash/{id}', 'PostController@trash');

    Route::put('posts/bulk-trash', 'PostController@bulkTrash');

    Route::put('posts/restore/{id}', 'PostController@restore');

    Route::put('posts/bulk-restore', 'PostController@bulkRestore');

    Route::put('posts/bulk-delete', 'PostController@bulkDelete');

    Route::get('posts/monthly-posts/{year}/{month}', 'PostController@monthlyPosts');

    Route::get('posts/yearly-posts/{year}', 'PostController@yearlyPosts');

    Route::resource('posts', 'PostController');

    Route::resource('pages', 'PageController');

    Route::resource('users', 'UserController');

    Route::put('user/update-password/{id}', 'UserController@updatePassword');

    Route::get('mails/{type}', 'EmailController@index');

    Route::put('mails/trash/{id}', 'EmailController@trash');

    Route::put('mails/bulk-trash', 'EmailController@bulkTrash');

    Route::put('mails/restore/{id}', 'EmailController@restore');

    Route::put('mails/bulk-restore', 'EmailController@bulkRestore');

    Route::put('mails/bulk-delete', 'EmailController@bulkDelete');

    Route::resource('mails', 'EmailController');

    Route::put('medias/bulk-delete', 'MediaController@bulkDelete');

    Route::resource('medias', 'MediaController');

    Route::resource('todo-items', 'ToDoItemController');
});

Route::get('/', 'FrontendPostController@getHomePage');

Route::get('/contact-us', function (Request $request) {
    visit($request->ip(), '/contact-us');
    return view('contact-us');
});

Route::get('/about-us', function (Request $request) {
    visit($request->ip(), '/about-us');
    return view('about-us');
});

Route::get('/news','FrontendPostController@getNews');

Route::get('/degrees/{slug}','PageController@show');

Route::get('/departments/{slug}', 'FrontendPageController@show');

Route::get('/{category}/{slug}', 'FrontendPostController@getPost');

Route::post('mails', 'EmailController@store');

// Route::get('/mail', function(){
//     $data = array('name'=>"Htet Myat", "body" => "Testing Email");

//     Mail::send('emails.testmail', $data, function($message) {
//         $message->to('htetmyathlaing@icloud.com', 'Htet Myat')
//                 ->subject('Test Mail')
//                 ->from('admin@ucsh.edu.mm','Universiyt of Computer Studies, Hinthada');
//     });
// });

function visit($ip_address, $url){
    // dd($request->server());
    // if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet
 //      $ip_address=$_SERVER['HTTP_CLIENT_IP'];
 //    }
 //    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
 //      $ip_address=$_SERVER['HTTP_X_FORWARDED_FOR'];
 //    }
 //    else{
 //      $ip_address=$_SERVER['REMOTE_ADDR'];
 //    }

    $visitor = new Visitor();
    $visitor->ip_address = $ip_address;
    $visitor->url = $url;
    $visitor->save();
}