<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	[
        		'name' => 'Faculty of Computer Science', 
        		'department' => 'Faculty of Computer Science',
                'slug' => str_slug('Faculty of Computer Science', '-')
        	],
        	[
        		'name' => 'Faculty of Computer System and Technology', 
        		'department' => 'Faculty of Computer System and Technology',
                'slug' => str_slug('Faculty of Computer System and Technology', '-')
        	],
        	[
        		'name' => 'Faculty of Computer Information Science', 
        		'department' => 'Faculty of Computer Information Science',
                'slug' => str_slug('Faculty of Computer Information Science', '-')
        	],
        	[
        		'name' => 'Faculty of Computing', 
        		'department' => 'Faculty of Computing',
                'slug' => str_slug('Faculty of Computing', '-')
        	],
        	[
        		'name' => 'Faculty of Computer Information Technology Support and Maintainance', 
        		'department' => 'Faculty of Computer Information Technology Support and Maintainance',
                'slug' => str_slug('Faculty of Computer Information Technology Support and Maintainance', '-')
        	],
        	[
        		'name' => 'Faculty of Natural Science', 
        		'department' => 'Faculty of Natural Science',
                'slug' => str_slug('Faculty of Natural Science', '-')
        	],
        	[
        		'name' => 'Faculty of Myanmar', 
        		'department' => 'Faculty of Myanmar',
                'slug' => str_slug('Faculty of Myanmar', '-')
        	],
        	[
        		'name' => 'Faculty of English', 
        		'department' => 'Faculty of English',
                'slug' => str_slug('Faculty of English', '-')
        	],
        	[
        		'name' => 'Student Affair', 
        		'department' => 'Student Affair',
                'slug' => str_slug('Student Affair', '-')
        	],
        	[
        		'name' => 'Finance', 
        		'department' => 'Finance',
                'slug' => str_slug('Finance', '-')
        	],
        	[
        		'name' => 'Library', 
        		'department' => 'Library',
                'slug' => str_slug('Library', '-')
        	],
            [
                'name' => 'Bachelor Degree',
                'slug' => str_slug('Bachelor Degree', '-'),
                'content' => '<div class="row">'.
                                '<div class="col-12 col-lg-6">'.
                                    '<h2 class="text-center py-4">B.C.Sc Course</h2>'.
                                    '<h4 class="text-center pb-3">First Year</h4>'.
                                    '<table class="table table-striped">'.
                                        '<tr>'.
                                            '<th>Subject Code</th>'.
                                            '<th>Field of Study</th>'.
                                        '</tr>'.
                                        '<tr>'.
                                            '<td>M</td>'.
                                            '<td>Myanmar</td>'.
                                        '</tr>'.

                                    '</table>'.
                                '</div>'.

                                '<div class="col-12 col-lg-6">'.
                                    '<h2 class="text-center py-4">B.C.Tech Course</h2>'.
                                    '<h4 class="text-center pb-3">First Year</h4>'.
                                    '<table class="table table-striped">'.
                                        '<tr>'.
                                            '<th>Subject Code</th>'.
                                            '<th>Field of Study</th>'.
                                        '</tr>'.
                                        '<tr>'.
                                            '<td>E</td>'.
                                            '<td>English</td>'.
                                        '</tr>'.
                                    '</table>'.
                                '</div>'.
                            '</div>'
            ],
            [
                'name' => 'Master Degree',
                'slug' => str_slug('Master Degree', '-'),
                'content' => '<div class="row">'.
                                '<div class="col-12 col-lg-6">'.
                                    '<h2 class="text-center py-4">M.C.Sc Course</h2>'.
                                    '<h4 class="text-center pb-3">First Year</h4>'.
                                    '<table class="table table-striped">'.
                                        '<tr>'.
                                            '<th>Subject Code</th>'.
                                            '<th>Field of Study</th>'.
                                        '</tr>'.
                                        '<tr>'.
                                            '<td>M</td>'.
                                            '<td>Myanmar</td>'.
                                        '</tr>'.

                                    '</table>'.
                                '</div>'.

                                '<div class="col-12 col-lg-6">'.
                                    '<h2 class="text-center py-4">M.C.Tech Course</h2>'.
                                    '<h4 class="text-center pb-3">First Year</h4>'.
                                    '<table class="table table-striped">'.
                                        '<tr>'.
                                            '<th>Subject Code</th>'.
                                            '<th>Field of Study</th>'.
                                        '</tr>'.
                                        '<tr>'.
                                            '<td>E</td>'.
                                            '<td>English</td>'.
                                        '</tr>'.
                                    '</table>'.
                                '</div>'.
                            '</div>'
            ],
        ];

        foreach ($roles as $role) {
        	App\Page::create($role);
        }
    }
}
