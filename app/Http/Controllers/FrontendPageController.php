<?php

namespace App\Http\Controllers;

use App\Page;
use App\Visitor;
use Illuminate\Http\Request;

class FrontendPageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $visitor = new Visitor();
        $visitor->ip_address = $request->ip();
        $visitor->url = '/'.$slug;
        $visitor->save();

        $page = Page::where('slug', $slug)->first();
        if($page)
            if($slug=='bachelor-degree' || $slug=='master-degree')
                return view('degree',compact('page'));
            else
                return view('department',compact('page'));
        else
            // redirect
            abort('404');
    }
}
