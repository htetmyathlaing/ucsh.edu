<?php

namespace App\Http\Controllers;

use App\Post;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public $months = array( 1  => 'January', 
                            2  => 'February', 
                            3  => 'March',
                            4  => 'April', 
                            5  => 'May',
                            6  => 'June', 
                            7  => 'July',
                            8  => 'August',
                            9  => 'September',
                            10 => 'October',
                            11 => 'November',
                            12 => 'December' );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $years = [];
        $postedYears = Post::all();
        foreach($postedYears->pluck('created_at')
                         ->reverse()
                         ->unique(function($date){ return $date->year; }) as $date){
            array_push($years, $date->year);
        }

        return response()->json( [
            'posts' => $this->yearlyPosts(Carbon::now()->year),
            'years' => $years,
        ]);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::where('slug' ,$request->input('slug'))->get();
       if(sizeOf($post))
                return 'false';

        $post = new Post();
        $post->title = $request->input('title');
        $post->category = $request->input('category');
        $post->department = $request->input('department');
        $post->type = $request->input('type');
        $post->slug = $request->input('slug');
        $post->feature_image = $request->input('feature_image');
        $post->content = $request->input('content');
        $post->save();
        return $post;
    }

    /**
     * Display the specified resource. to admin panel
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if($post)
            return $post;

        abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        if($post->slug != $request->input('slug')){
            $oldPost = Post::where('slug', $request->input('slug'))->get();
            if(sizeOf($oldPost))
                return 'false';
        }

        // $post->title = $request->input('title');
        // $post->category = $request->input('category');
        // $post->department = $request->input('department');
        // $post->type = $request->input('type');
        // $post->slug = $request->input('slug');
        // $post->feature_image = $request->input('feature_image');
        // $post->content = $request->input('content');
        // $post->save();
        return 'true';
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trash($id)
    {
        $post = Post::findOrFail($id);
        $post->type = 'trash';
        $post->save();
        return "true";
    }

    /**
     * Trash the specified resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkTrash(Request $request)
    {
        $ids = $request->posts;
        foreach ($ids as $id) {
           $post = Post::findOrFail($id);
           $post->type = 'trash';
           $post->save();
        }
        return "true"; 
    }

    /**
     * restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $post = Post::findOrFail($id);
        $post->type = 'draft';
        $post->save();
        return "true";
    }

    /**
     * Restore the specified resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkRestore(Request $request)
    {
        $ids = $request->posts;
        foreach ($ids as $id) {
           $post = Post::findOrFail($id);
           $post->type = 'draft';
           $post->save();
        }
        return "true"; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return "true";
    }


    /**
     * Romove the specified resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDelete(Request $request)
    {
        $ids = $request->posts;
        foreach ($ids as $id) {
           $post = Post::findOrFail($id);
           $post->delete();
        }
        return "true"; 
    }

    public function yearlyPosts($year){
        if (auth()->user()->can('administer'))
            return Post::whereYear('created_at', $year)
                       ->latest()
                       ->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);

        else
            return Post::where('department', auth()->user()->role->name)
                       ->whereYear('created_at', $year)
                       ->latest()
                       ->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);
    }

    public function monthlyPosts($year, $month){
        if($month == 'All'){
            if (auth()->user()->can('administer'))
                return Post::whereYear('created_at', $year)
                           ->latest()
                           ->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);

            return Post::where('department', auth()->user()->role->name)
                       ->whereYear('created_at', $year)
                       ->latest()->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);
        }
        
        $month = array_search($month, $this->months);
        if (auth()->user()->can('administer'))
            return Post::whereYear('created_at', $year)
                       ->whereMonth('created_at', $month)
                       ->latest()
                       ->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);

        else
            return Post::where('department', auth()->user()->role->name)
                       ->whereYear('created_at', $year)
                       ->whereMonth('created_at', $month)
                       ->latest()
                       ->get(['id', 'title', 'category', 'department', 'type', 'slug', 'created_at']);
    }
}
