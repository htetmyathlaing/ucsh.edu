<?php

namespace App\Http\Controllers;

use App\Post;
use App\Visitor;
use Illuminate\Http\Request;

class FrontendPostController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPost(Request $request, $category, $slug)
    {
        $visitor = new Visitor();
        $visitor->ip_address = $request->ip();
        $visitor->url = '/'.$category.'/'.$slug;
        $visitor->save();

        $post = Post::where('slug', $slug)->first();
        $otherPosts = Post::where('category', $category)->latest()->limit(5)->get();
        if($post)
            return view('post', compact('post', 'otherPosts'));

        abort('404');
    }

    // get all posts
    public function getNews(Request $request){
        $visitor = new Visitor();
        $visitor->ip_address = $request->ip();
        $visitor->url = '/news';
        $visitor->save();

        $posts = Post::paginate(10);
        return view('news', compact('posts'));
    }

    // get posts for home page 
    public function getHomePage(Request $request){
        $visitor = new Visitor();
        $visitor->ip_address = $request->ip();
        $visitor->url = '/';
        $visitor->save();

        $news = Post::where('category', 'news')
                          ->where('type', 'published')
                          ->latest()
                          ->limit(5)
                          ->get();

        $activities = Post::where('category', 'activities')
                          ->where('type', 'published')
                          ->latest()
                          ->limit(5)
                          ->get(['id', 'title', 'category', 'slug', 'feature_image', 'created_at']);

        $announcements = Post::where('category', 'announcements')
                          ->where('type', 'published')
                          ->latest()
                          ->limit(5)
                          ->get(['id', 'title', 'category', 'slug', 'feature_image', 'created_at']);

        return view('index', compact('news', 'activities', 'announcements'));
    }
}
