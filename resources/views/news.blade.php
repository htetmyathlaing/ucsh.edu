@extends('layout')

@section('content')
    <!-- about us image -->

<section>
    <div id="about-us-header" style="background: url({{asset('img/slider.png')}});">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 offset-sm-2"> 
                    <div class="header-text-wrap">
                        <div class="inner-header-text">
                            <h1>News & Events & Announment</h1>
                        </div>
                    </div>
                </div>                     
            </div>
        </div>
    </div>
</section>
    <!-- end about us image -->
<section id="news-pg" class="py-4">
    <div class="container">
        <div class="row">
            @foreach($posts as $index=>$post) 
            <div class="col-md-4 mb-4">
                <div class="new-box">

                    @if($post->feature_image==null)
                        <img src="{{ asset("/img/logo.png") }}" class="img-fluid feature-image" alt="{{ $post->title }}">
                    @else
                        <img src="{{ asset($post->feature_image) }}" class="img-fluid feature-image" alt="{{ $post->title }}">
                    @endif

                    <div class="p-4 text-bgcolor">
                        <h5>
                            <a href="/{{ $post->category.'/'.$post->slug }}" title="{{ $post->title }}">
                                {{ $post->title }}
                            </a>
                        </h5>
                        <p class="new-date">{{ $post->created_at->toFormattedDateString()}}</p>
                        <p>{{ str_limit(strip_tags($post->content), 150, '...') }}</p>

                        <div class="text-center">
                            <a href="/{{ $post->category.'/'.$post->slug }}" title="{{ $post->title }}">
                                @if($index%2==0)
                                    <button class="btn btn-danger">Read More</button>
                                @else
                                    <button class="button button--rayen button--border-thin button--text-thick button--text-upper button--size-s" data-text="Read More"><span>Read More</span></button>
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <!-- pagination -->
        {{ $posts->links() }}
        <!-- end pagination -->
    </div>
</section>
@endsection

@section('css')
    .feature-image{
        width: 100% !important;
        height: 250px !important;
    }
@endsection