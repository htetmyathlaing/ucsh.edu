<?php

namespace App\Http\Controllers;

use App\Post;
use App\Page;
use App\User;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public $months = array( 1  => 'January', 
                            2  => 'February', 
                            3  => 'March',
                            4  => 'April', 
                            5  => 'May',
                            6  => 'June', 
                            7  => 'July',
                            8  => 'August',
                            9  => 'September',
                            10 => 'October',
                            11 => 'November',
                            12 => 'December' );
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function init()
    {
    	$years = [];
        $visitors = Visitor::all();
    	foreach($visitors->pluck('created_at')
    					 ->reverse()
			    		 ->unique(function($date){ return $date->year; }) as $date){
			array_push($years, $date->year);
		}

		return response()->json( [
			'posts' => Post::count(),
			'pages' => Page::count(),
            'users' => User::count(),
			'todayVisitors' => Visitor::whereDate('created_at', '>=', Carbon::today())
			                       ->get()
			                       ->unique(function($visitor){ return $visitor->ip_address; })//same ip_address as 1
			                       ->count(),
			'years' => $years,

			'trafficData' => $this->yearlyTraffic(Carbon::now()->year),
			'postsUpdateRateData' => $this->yearlyPostsUpdateRate(Carbon::now()->year),
		]);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function yearlyTraffic($year)
    {
        $trafficData = [];
        $visitors = Visitor::whereYear('created_at', $year);
        for($month = 1; $month <= 12; $month++){
            array_push($trafficData, Visitor::whereYear('created_at', $year)
                                            ->whereMonth('created_at', $month)
                                            ->get()
                                            ->unique(function($visitor){ return $visitor->ip_address; })
                                            ->count());
        }
        return $trafficData;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function monthlyTraffic($year, $month)
    {
        $trafficData = [];
        $month = array_search($month, $this->months);

        if($month == 2 ){
            for($day = 1; $day <= 28; $day++){
                array_push($trafficData, Visitor::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->get()
                                                ->unique(function($visitor){ return $visitor->ip_address; })
                                                ->count());
            }
            return $trafficData;
        }
        elseif($month == 4 || $month == 6 || $month == 9 || $month == 11){
            for($day = 1; $day <= 30; $day++){
                array_push($trafficData, Visitor::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->get()
                                                ->unique(function($visitor){ return $visitor->ip_address; })
                                                ->count());
            }
            return $trafficData;
        }
        else{
            for($day = 1; $day <= 31; $day++){
                array_push($trafficData, Visitor::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->get()
                                                ->unique(function($visitor){ return $visitor->ip_address; })
                                                ->count());
            }
            return $trafficData;
        }
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function yearlyPostsUpdateRate($year)
    {
    	$rate = [];
        for($month = 1; $month <= 12; $month++){
            array_push($rate, Post::whereYear('created_at', $year)
                                            ->whereMonth('created_at', $month)
                                            ->count());
        }
        return $rate;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function monthlyPostsUpdateRate($year, $month)
    {
        $rate = [];
        $month = array_search($month, $this->months);
        // return $month;

        if($month == 2 ){
            for($day = 1; $day <= 28; $day++){
                array_push($rate, Post::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->count());
            }
            return $rate;
        }
        elseif($month == 4 || $month == 6 || $month == 9 || $month == 11){
            for($day = 1; $day <= 30; $day++){
                array_push($rate, Post::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->count());
            }
            return $rate;
        }
        else{
            for($day = 1; $day <= 31; $day++){
                array_push($rate, Post::whereYear('created_at', $year)
                                                ->whereMonth('created_at', $month)
                                                ->whereDay('created_at', $day)
                                                ->count());
            }
            return $rate;
        }
    }
}
