@extends('layout')

@section('content')
<!-- <div id="add-listings-inner-header" class="depart-list">
   <div class="container">
        <div class="row">
             <div class="col-sm-8 offset-sm-2"> 
                  <div class="header-text-wrap">
                       <div class="inner-header-text">
                                <h1>Faculty Of Computer Science</h1>
                       </div>
                  </div>
             </div>                                  
        </div>
   </div>
</div> -->

  <section id="department-header" style="background: url({{asset($page->feature_image)}});">
     <div class="container">
          <div class="row">
               <div class="col-sm-10 col-md-8 offset-sm-2"> 
                    <div class="header-text-wrap">
                         <div class="inner-header-text">
                            <h1>{{ $page->name }}</h1>  <!-- fill data -->
                         </div>
                    </div>
               </div>                     
          </div>
     </div>
  </section>
  <!-- end about us image -->
<section id="dept-pg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
            <h2 class="text-center mt-2 py-md-4 pb-4">
                {{ $page->name }} Teachers
            </h2>
                <div class="row">
                    @if(strlen($page->teacher_photos))
                        @foreach(json_decode($page->teacher_photos) as $photo)
                            <div class="col-12 col-sm-6 col-md-4 mb-4">
                                <div class="tech-whole">
                                <a data-fancybox="gallery" href="/{{ $photo->photoUrl }}" style="text-decoration: none;">
                                    <img src="/{{ $photo->photoUrl }}" class="img-fluid t-img" alt="teacher-image">
                                    <div class="teacher-name text-center">
                                        <span>{{ $photo->name }}</span><br>
                                        <span>{{ $photo->academicDegree }}</span><br>
                                        <span>{{ $photo->position }}</span>
                                    </div>
                                </a>
                                </div>
                            </div>
                       @endforeach
                    @endif
                </div>
                <div class="container">
                {!! $page->content !!}
                </div>

            </div>
            <div class="col-12 col-lg-4">
                <div class="container p-0">
                <div class="row">
                    <div class="col-12">
                        <div class="mt-3 py-4">
                            <h3 class="text-center">Department</h3>
                        </div>

                        <ul class="sidebar-content">
                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">Myanmar Department</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>

                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">English Department</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>

                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">Natural Science</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>

                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">Faculty of Computing</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>

                            <li class="each-lp">
                                <a href="department.php" title="dept-name">
                                    <span class="dept-name">Faculty of Computer Science</span>
                                    <div style="background-image: url(/img/sd.png);" class="slide-img"></div>
                                </a>                               
                            </li>
                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">Faculty of Information Science</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>
                            <li class="each-lp">
                                <a href="#" title="dept-name">
                                    <span class="dept-name">Faculty of Computer System and Technologies</span>
                                    <div style="background-image: url(/img/slider.png);" class="slide-img"></div>
                                </a>                               
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection