@extends('layout')
@section('content')
<section id="degree-header" style="background: url({{asset('img/slider.png')}});">
   <div class="container">
        <div class="row">
             <div class="col-sm-8 offset-sm-2"> 
                  <div class="header-text-wrap">
                       <div class="inner-header-text">
                            <h1>{{ $page->name }}</h1>  <!-- fill data -->
                       </div>
                  </div>
             </div>                     
        </div>
   </div>
</section>

<section id="bcs-bct" class="pb-3">
	<div class="container-fluid">
		{!! $page->content !!}
	</div>
</section>

@endsection