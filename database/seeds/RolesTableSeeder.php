<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	['name' => 'Admin'],
        	['name' => 'Faculty of Computer Science'],
        	['name' => 'Faculty of Computer System and Technology'],
        	['name' => 'Faculty of Computer Information Science'],
        	['name' => 'Faculty of Computing'],
        	['name' => 'Faculty of Computer Information Technology Support and Maintainance'],
        	['name' => 'Faculty of Natural Science'],
        	['name' => 'Faculty of Myanmar'],
        	['name' => 'Faculty of English'],
        	['name' => 'Student Affair'],
        	['name' => 'Finance'],
        	['name' => 'Library'],
        ];

        foreach ($roles as $role) {
        	App\Role::create($role);
        }
    }
}
