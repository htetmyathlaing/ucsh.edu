@extends('layout')
@section('content')
<section id="error-pg" style="background: url(img/slider.png);">
       <div class="container">
            <div class="row">
                 <div class="col-sm-8 offset-sm-2"> 
                      <div class="header-text-wrap">
                           <div class="inner-header-text">
                       			<p>Some thing wrong!</p>
                                <h1>404</h1>
                                <a href="#">Back To Home</a>
                           </div>
                      </div>
                 </div>                     
            </div>
       </div>
</section>

@endsection