<?php

namespace App\Http\Controllers;

use App\Email;
use App\Mail\SentMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type='')
    {
        if (auth()->user()->can('administer')){
        	switch ($type) {
        		case "stared":
        			return Email::where('stared', true)->latest()->paginate(50);
        			break;

                case "sent":
                    return Email::where('sent', true)->latest()->paginate(50);
                    break;

        		case "trashed":
        			return Email::onlyTrashed()->latest()->paginate(50);
        			break;
        		
        		default:
        			return Email::where('sent', false)->latest()->paginate(50);
        			break;
        	}
        }
        abort('403'); //unauthorized
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = new Email();
        $email->email = $request->input('email');
        $email->phone = $request->input('phone');
        $email->subject = $request->input('subject');
        $email->message = $request->input('message');
        $email->stared = false;
        if($request->input('sent')){
            $email->sent = true;
            $email->name = 'Admin';
        }
        else{
            $email->sent = false;
            $email->name = $request->input('name');
        }
        $email->save();

        if($email->sent){
            $message = ['subject'=> $email->subject, "body" => $email->message];

            Mail::to($email->email)->queue(new SentMail($message));
            return 'Email Sent';
        }

        return redirect()->back()->with('flash_message', 'Successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function show(Email $email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->user()->can('administer')){
        	$email = Email::findOrFail($id);
        	$email->stared = !$email->stared;
        	$email->save();
            return ''.$email->stared;
        }
        abort('403'); // unauthorized
    }

    /**
     * Trash the specified resource in storage.
     *
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function trash($id)
    {
        if (auth()->user()->can('administer')){
        	$email = Email::findOrFail($id);
            $email->delete();
            return "true";
        }
        abort('403'); // unauthorized
    }

    /**
     * Trash the specified resources from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkTrash(Request $request)
    {
        if (auth()->user()->can('administer')){
            Email::destroy($request->mails);
            return "true";
        }
        abort('403'); // unauthorized
    }

    /**
     * Restore the specified resource in storage.
     *
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (auth()->user()->can('administer')){
        	$email = Email::onlyTrashed()->findOrFail($id);
            $email->restore();
            return "true";
        }
        abort('403'); // unauthorized
    }

    /**
     * Restore the specified resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkRestore(Request $request)
    {
        if (auth()->user()->can('administer')){
            $ids = $request->mails;
            foreach ($ids as $id) {
               $email = Email::onlyTrashed()->where('id', $id);
               $email->restore(); 
            }
            return "true"; 
        }
        abort('403'); // unauthorized
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        if (auth()->user()->can('administer')){
        	$email->forceDelete();
            return "true";
        }
        abort('403'); // unauthorized
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDelete(Request $request)
    {
        if (auth()->user()->can('administer')){
            $ids = $request->mails;
            foreach ($ids as $id) {
               $email = Email::onlyTrashed()->where('id', $id);
               $email->forceDelete(); 
            }
            return "true"; 
        }
        abort('403'); // unauthorized
    }
}
