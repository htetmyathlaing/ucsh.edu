@extends('layout')

@section('content')
  <section>
    <div id="about-us-header" style="background: url({{asset('img/slider.png')}});">
       <div class="container">
            <div class="row">
                 <div class="col-sm-8 offset-sm-2"> 
                      <div class="header-text-wrap">
                           <div class="inner-header-text">
                                    <h1>About Us</h1>
                           </div>
                      </div>
                 </div>                     
            </div>
       </div>
    </div>
  </section>
  <!-- end about us image -->

  <!-- About University -->
  <section id="about-uni">
    <div class="container">
      <h2 class="text-center about-title">About University</h2>
      <div class="row">
        <div class="col-12">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- end about university -->

  <!-- vision and mission -->
  <section id="vision-mission" class="py-4">
    <div class="container">
      <h2 class="text-center mb-3 vm-title">Our Vision and Misson</h2>
      <div class="row">
        <div class="col-md-6 mb-3 mb-md-0">
        <div class="mission-wrap">
          <div class="mission-border">
            <h3 class="text-center">Our Mission</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="vision-wrap">
          <div class="vision-border">
            <h3 class="text-center">Our Vision</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>
  <!-- end vision and mission -->
  
  <!-- about developer -->
  
  <!-- end about developer -->
@endsection